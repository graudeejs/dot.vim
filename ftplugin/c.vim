setlocal path+=/usr/include/,/usr/local/include/,./include/

nmap <F1> <ESC>:Man 3 <C-R>=expand("<cword>")<CR><CR>

if has("cscope")
  let cscope_path = ProgrPath(['cscope'])
  if cscope_path != ''
    let &csprg = cscope_path

    setlocal ts=4
    setlocal sw=4
    setlocal noexpandtab
    setlocal csto=0
    setlocal cst
    setlocal nocsverb
    if filereadable("cscope.out")
      cs add cscope.out
    elseif filereadable(".cscope.out")
      cs add .cscope.out
    endif
    setlocal csverb

    " Find this C symbol
    nmap <C-_>s :cs find s <C-R>=expand("<cword>")<CR><CR>

    " Find this definition
    nmap <C-_>g :cs find g <C-R>=expand("<cword>")<CR><CR>

    " Find functions called by this function
    nmap <C-_>d :cs find d <C-R>=expand("<cword>")<CR><CR>

    " Find functions calling this function
    nmap <C-_>c :cs find c <C-R>=expand("<cword>")<CR><CR>

    " Find this text string
    nmap <C-_>t :cs find t <C-R>=expand("<cword>")<CR><CR>

    " Find this egrep pattern
    nmap <C-_>e :cs find e <C-R>=expand("<cword>")<CR><CR>

    " Find this file
    nmap <C-_>f :cs find f <C-R>=expand("<cfile>")<CR><CR>

    " Find this file
    nmap <C-_>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
  endif
endif

call LoadIfNeeded(['a'])
