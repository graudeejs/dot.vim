let b:python = ProgrPath(['python3', 'python'])
execute 'map' '<buffer><silent> = !' . b:python . ' -mjson.tool<cr>'
setlocal ts=4
setlocal sw=4
