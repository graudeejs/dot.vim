setlocal ts=2 sw=2 expandtab
setlocal spell spelllang=en_gb
setlocal textwidth=78
setlocal colorcolumn=79
setlocal formatoptions=tcrqnl1
