set nocompatible

if has('python3')
  set pyx=3
elseif has('python')
  set pyx=2
endif

"{{{ System2

function! System2(exec_app)
  return substitute(system(a:exec_app), "[\r|\n]$", '', '')
endfunction

"}}}

if exists('$OS')
  let g:os = $OS
else
  let g:os = System2('uname')
endif

if exists('$WHOAMI')
  let whoami = $WHOAMI
else
  let whoami = System2('whoami')
endif

let user_id = System2('id -u')

" https://github.com/vim-ruby/vim-ruby/issues/236
if !has('nvim') && !g:os == 'Darwin'
  set noballooneval
endif
let g:netrw_nobeval = 1



" LoadIfNeeded {{{
function! LoadIfNeeded(plugin_name_list)

  if !exists('g:load_if_needed_plugins')
    let g:load_if_needed_plugins = {
          \ 'a':              'g:loaded_alternateFile',
          \ 'ack':            'g:loaded_ack',
          \ 'ctrlp':          'g:loaded_ctrlp',
          \ 'easygrep':       'g:EasyGrepVersion',
          \ 'fuzzyfinder':    '0',
          \ 'gitv':           'g:loaded_gitv',
          \ 'indent-guides':  'g:loaded_indent_guides',
          \ 'l9':             'g:loaded_autoload_l9',
          \ 'matchit':        'g:loaded_matchit',
          \ 'rails':          'g:loaded_rails',
          \ 'tagbar':         'g:loaded_tagbar',
          \ 'vcscommand':     'g:loaded_VCSCommand',
          \ 'ctrlp-modified': 'g:loaded_ctrlp_modified',
          \ 'ctrlp-smarttabs': 'g:loaded_ctrlp_smarttabs',
          \ 'ctrlp-funky':    'g:loaded_ctrlp_funky'
          \ }
  endif

  for plugin_name in a:plugin_name_list
    let check_var = g:load_if_needed_plugins[plugin_name]
    if check_var == '0'
      g:load_if_needed_plugins[plugin_name] = '1'
      execute ":IP " . plugin_name
    elseif check_var != '1'
      if !exists(check_var)
        execute ":IP " . plugin_name
      end
    end
  endfor

endfunction

"}}}

" MkdirIfNeeded(dir, flags, permissions) {{{

function! MkdirIfNeeded(dir, flags, permissions)
  if !isdirectory(a:dir)
    call mkdir(a:dir, a:flags, a:permissions)
  endif
endfunction

"}}}
" ToggleSyntaxOrSpecChars() {{{

function! ToggleSyntaxOrSpecChars()
  if &l:syntax != 'OFF'
    setlocal syntax=OFF
    setlocal colorcolumn=80
    setlocal list
  else
    setlocal syntax=ON
    setlocal colorcolumn=
    setlocal nolist
  endif
endfunction

"}}}
" DoIHaveDotDirInPath() {{{

function! DoIHaveDotDirInPath()
  for path in split(substitute(getcwd(), '/', ' ', 'g'))
    if path =~ '\..'
      return 1
    endif
  endfor
  return 0
endfunction

"}}}
" PromptRemoveTrainingWhitespace() {{{

function! PromptRemoveTrainingWhitespace()
  if &filetype != 'gitcommit'
    let has_trailing_spaces=!!search('\v\s+$', 'cwn')
    if has_trailing_spaces
      if ! exists("b:remove_trailing_whitespace")
        let has_trailing_spaces=!!search('\v\s+$', 'cwn')
        if has_trailing_spaces
          let choice = confirm("Remove trailing whitespace?", "&Yes\n&No")
          if choice == 1
            call Trim()
            let b:remove_trailing_whitespace = 1
          else
            let b:remove_trailing_whitespace = 0
          endif
        endif
      elseif b:remove_trailing_whitespace == 1
        call Trim()
      endif
    endif
  endif
endfunction

"}}}
" PromptToConvertToUnixFormat() {{{

function! PromptToConvertToUnixFormat()
  if &fileformat != 'unix'
    if ! exists("b:convert_to_unix_fileformat")
      let b:convert_to_unix_fileformat = confirm("Convert buffer to unix format?", "&Yes\n&No")
      if b:convert_to_unix_fileformat == 1
        setlocal fileformat=unix
      end
    end

    if b:convert_to_unix_fileformat == 1
      setlocal fileformat=unix
    end
  end
endfunction

"}}}
" Trim() {{{

function! Trim()
  let cur_linenr = line('.')
  let cur_col = col('.')
  let _s=@/
  %s/\s\+$//e
  let @/=_s
  nohl
  call cursor(cur_linenr, cur_col)
endfunction

"}}}
" TrimRange() {{{

function! TrimRange() range
  exec a:firstline.",".a:lastline."substitute /\\v\\s+$//e"
endfunction

" }}}
" ProgrPath(prog_list) {{{

function! ProgrPath(prog_list)
  " return path to first program in list

  let paths = split($PATH, ':')

  for prog_name in a:prog_list
    for path in paths
      let prog_file = path . '/' . prog_name
      if executable(prog_file)
        return prog_file
      endif
    endfor
  endfor

  return ''
endfunction

"}}}
" ProjectDir {{{

function! ProjectDir()
  " return path of project directory or '.'

  if exists("$PROJECT_DIR")
    return $PROJECT_DIR
  endif

  let exp = '%:p:h'
  let dir = expand(exp)

  while dir != '/'
    if filereadable(dir . '.git')
      " git submodule
      return dir
    endif
    for psign in ['.git', '.hg', '.svn', '.cvs', '.bzr']
      if isdirectory(dir . '/' . psign)
        return dir
      endif
    endfor
    let exp = exp . ':h'
    let dir = expand(exp)
  endwhile

  return '.'
endfunction

"}}}
" ProjectName {{{

function! ProjectName()
  let project_dir = ProjectDir()
  if project_dir == '.'
    return 'UNTITLED'
  else
    return substitute(split(project_dir, '/')[-1], '\.', '_', '')
  end
endfunction

"}}}

let realhome = $HOME

if g:os == 'Darwin'
  let realhome = '/Users/' . whoami
  let macvim_skip_colorscheme = 1
  let macvim_hig_shift_movement = 1
elseif filereadable('/etc/passwd')
  let realhome = system("awk 'BEGIN { FS=\":\"; ORS=\"\" }; /^" . whoami . ":/ {print $6}' /etc/passwd")
endif

if isdirectory("/run/user/" . user_id)
  let vim_tmp = "/run/user/" . user_id . "/vim"
else
  let vim_tmp = realhome . "/tmp/.vim"
endif
call MkdirIfNeeded(vim_tmp, 'p', 0700)

let vim_cache = realhome . '/.cache/vim'
call MkdirIfNeeded(vim_cache, 'p', 0700)

if has('gui_running') == 0 && $TERM != 'cons25'
  set ttyfast
endif

syntax on

if $TERM == 'xterm' || $TERM == 'xterm-256color' || $TMUX != ''
  " might also work for urxvt
  set t_ti= t_te=
endif

if $TERM == 'xterm-256color' || $TERM == 'rxvt-unicode-256color' || $TERM == 'st-256color'
  set t_Co=256
endif

set selectmode=mouse
set showtabline=2
set splitbelow
" set synmaxcol=100
" set virtualedit=block
if has('gui_running') == 1
  set antialias
endif
set autoindent
set autoread
set backspace=indent,start,eol
set browsedir=buffer
set bufhidden=hide
" set cinoptions=>s,e0,n0,f0,{0,}0,^0,L-1,:0,=s,l0,b0,gs,hs,ps,t0,is,+s,c3,C0,/0,(2s,us,U0,w0,W0,m0,j0,J0,)20,*70,#0
set cmdwinheight=14
if !has('nvim')
  set cryptmethod=blowfish
endif
set display+=uhex
set encoding=utf-8
set equalalways
set foldnestmax=3
set foldopen=tag,hor,insert,mark,quickfix,search,undo
set guioptions=acigtMe
set helplang=en
set hidden
set history=50
set incsearch
set listchars=tab:\\_,eol:$
set matchtime=15
set modeline
set modelines=5
set mouse=nv
set mousehide
set noautowrite
set noexpandtab
set noexrc
set noshowmatch
set nrformats=alpha,octal,hex
set previewheight=8
set ruler
set scrolljump=-15
set scrolloff=5
set sessionoptions+=unix,slash
set shiftround
set showbreak=}>
set showcmd
set showmode
set sidescrolloff=10
set smartcase
set smarttab
set splitright
set statusline=%<%f\ %y%w%q%m%r%=%-14.(%l,%c%V%)\ %P
set tagstack
set title
set titleold=
set viewoptions+=unix,slash
set wildmenu
set wildmode=longest:list
set winaltkeys=no
set wrapscan
set clipboard+=unnamed
"set diffopt+=iwhite,vertical,context:10
set spell

set noswapfile

" set showfulltag

set shiftwidth=2
set expandtab

call MkdirIfNeeded(vim_cache . '/backup', 'p', 0700)
let &backupdir = vim_cache . '/backup'
set backup

set formatoptions+=rcqnl
set formatoptions-=to

set wildignore+=*.aux,*.out,*.toc                       " LaTeX intermediate files
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg          " binary images
set wildignore+=*.luac                                  " Lua byte code
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.so   " compiled object files
set wildignore+=*.pyc                                   " Python byte code
set wildignore+=*.spl                                   " compiled spelling word lists
set wildignore+=*.swp,*.swo                             " Vim swap files
set wildignore+=*.DS_Store?                             " OSX bullshit
set wildignore+=*.jar                                   " Java
set wildignore+=*.pdf,*.dvi,*.ps                        " Documents

set suffixes=.aux,.out,.toc.jpg,.bmp,.gif,.png,.jpeg,.luac,.o,.obj,.exe,.dll,.manifest,.so,.pyc,.spl,.swp,.swo,.DS_Store?,.jar,.pdf,.dvi,.ps

call MkdirIfNeeded(vim_cache . '/swap', 'p', 0700)
let &directory = vim_cache . '/swap,.'

set undofile
call MkdirIfNeeded(vim_tmp . '/undo', 'p', 0700)
let &undodir = vim_tmp . '/undo'

if !has('nvim')
let &viminfo .= ",r~/mnt,r/mnt,r/media,r/cdrom,<500,f1,'200,n" . vim_cache . "/viminfo"
endif

" Resize vindows if vim is resized
autocmd VimResized * exe "normal! \<c-w>="

autocmd BufWritePre * call PromptRemoveTrainingWhitespace()
autocmd BufWritePre * call PromptToConvertToUnixFormat()

if isdirectory('/usr/share/dict')
  for dictionary in ['words', 'web2a', 'propernames', 'freebsd']
    if filereadable('/usr/share/dict/' . dictionary)
      execute "set dictionary+=/usr/share/dict/" . dictionary
    endif
  endfor
endif

" set noautochdir

" Russian phonetic to English
set langmap=ю`,ч=,яq,вw,еe,рr,тt,ыy,уu,иi,оo,пp,ш[,щ],аa,сs,дd,фf,гg,хh,йj,кk,лl,зz,ьx,цc,жv,бb,нn,мm,э\\,Ю~,ё#,Ё$,ъ%,Ъ^,Ч+,ЯQ,ВW,ЕE,РR,ТT,ЫY,УU,ИI,ОO,ПP,Ш{,Щ},АA,СS,ДD,ФF,ГG,ХH,ЙJ,КK,ЛL,ЗZ,ЬX,ЦC,ЖV,БB,НN,МM,Э\|

if g:os == 'Darwin'
  set guifont=Monaco:h16
else
  set guifont=DejaVu\ Sans\ Mono\ 13
end

" set linespace=0

if $TERM != 'linux'
  behave xterm
end
filetype plugin indent on

let g:pathogen_disabled = [] " disabled vim scritps
" MISC {{{

" brew install ctags-exuberant for mac
let ctags_progr = ProgrPath(['exctags', 'ctags'])
let find_progr = ProgrPath(['find'])

"}}}
" VimBall {{{

" I don't use it, so let's disable it
let g:loaded_vimballPlugin = 1
let g:loaded_vimball = 1
let g:vimball_home= realhome . "/.vim/bundle/vimball"

" }}}
" Ack {{{

if ProgrPath(['ag']) != '' " the_silver_searcher
  let g:ackprg = 'ag --nogroup --nocolor --column --vimgrep'
  nmap <leader>A <ESC>:Ack <cword><CR>
elseif ProgrPath(['ack', 'ack-grep']) != '' " ack
  " no customization
  nmap <leader>A <ESC>:Ack! <cword><CR>
elseif ProgrPath(['pt']) != '' " the_platinum_searcher
  let g:ackprg = 'pt --nogroup --nocolor'
  nmap <leader>A <ESC>:Ack <cword><CR>
else
  " disable ack, if there is no ack/pt/ag executable
  call add(g:pathogen_disabled, "ack.vim")
endif

"}}}
" nerd commenter {{{

let g:NERDDefaultNesting = 1
let g:NERDMenuMode = 0
let g:NERDSpaceDelims = 1
let g:NERDBlockComIgnoreEmpty = 0
let g:NERDCommentWholeLinesInVMode = 1
let g:NERDBlockComIgnoreEmpty = 0

let g:NERDDefaultAlign = 'left'
let g:NERDCustomDelimiters = { 'cf3': { 'left': '#' }, 'nix': { 'left': '#' } }

"}}}
" FuzzyFind plugin {{{

if has('tag_binary') && exists("ctags_progr")
  let g:fuf_buffertag_ctagsPath = ctags_progr " FuzzyFind
else
  if !exists("g:fuf_modesDisable")
    let g:fuf_modesDisable = []
  endif
  call add(g:fuf_modesDisable, 'tag')
  call add(g:fuf_modesDisable, 'buffertag')
endif
let g:fuf_timeFormat = '(%Y.%m.%d %H:%M:%S)'
let g:fuf_dataDir = vim_tmp . '/fuf'
call MkdirIfNeeded(g:fuf_dataDir, 'p', '0700')
let g:fuf_file_exclude = '\v\~$|\.(o|exe|dll|bak|orig|swp)$|(^|[/\\])\.(hg|git|bzr|svn)($|[/\\])'
let g:fuf_dir_exclude = '\v(^|[/\\])\.(hg|git|bzr|svn)($|[/\\])'
let g:fuf_mrufile_exclude = '\v\~$|\.(o|exe|dll|bak|orig|sw[po])$|^(\/\/|\\\\|\/mnt\/|\/media\/)'

"}}}
" SuperTab plugin {{{

let g:SuperTabLongestEnhanced = 1
set completeopt=menuone,longest
if has('gui_running')
  let g:SuperTabMappingForward = '<C-Space>'
  let g:SuperTabMappingBackward = '<S-C-Space>'
else
  let g:SuperTabMappingForward = '<Nul>'
  let g:SuperTabMappingBackward = '<S-Nul>'
endif

"}}}
" ctrlp plugin {{{

let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_switch_buffer = 'eT'
let g:ctrlp_max_depth = 20
let g:ctrlp_max_height = 20
let g:ctrlp_mruf_max = 150
let g:ctrlp_working_path_mode = 'c'
let g:ctrlp_buftag_ctags_bin = ctags_progr
let g:ctrlp_map = ''
let g:ctrlp_tabpage_position = "ac"
let g:ctrlp_show_hidden = 0


let g:ctrlp_extensions = ['tag']

let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
call MkdirIfNeeded(g:ctrlp_cache_dir, 'p', 0700)

if $TERM == 'xterm' || $TERM == 'xterm-256color'
  let g:ctrlp_prompt_mappings = {
    \ 'PrtBS()':      ['<bs>', '<c-]>', '<c-h>'],
    \ 'PrtCurLeft()': ['<left>', '<c-^>'],
    \ }
endif

if exists("find_progr") && g:os != 'Minix'
  let egrep_v_files = '\.(swp|swo|gitkeep|keepme|so|o)$'
  let egrep_vi_files = '\.(7z|7|avi|bak|bin|bmp|db|docx|doc|exe|flac|flv|gif|ico|img|iso|jar|jpg|log|mkv|mp3|odg|ods|odt|pdf|plt|png|ppt|rar|tgz|wav|wma|wmv|xsl|zip|out|meta|DS_store|pyc|beam)$'

  if DoIHaveDotDirInPath() == 0
    " Use this if we're in normal directory
    " This will not search in hidden directories
    let g:ctrlp_user_command = find_progr . " %s '(' -type f -or -type l ')' -maxdepth " . g:ctrlp_max_depth . " -not -path '*/\.*/*' | egrep -v '/(coverage|tmp|cache|__pycache__|node_modules)/' | egrep -v '" . egrep_v_files . "' | egrep -i -v '" . egrep_vi_files . "' | sort"
  else
    " This will be used if we're in (or parent is) hidden direcotry
    " This will search in hidden directories, but not in \.(git|hg|svn)
    let g:ctrlp_user_command = find_progr . " %s '(' -type f -or -type l ')' -maxdepth " . g:ctrlp_max_depth . " | egrep -v '" . egrep_v_files . "' | egrep -v '/\.(git|hg|svn|bzr)/' | egrep -i -v '" . egrep_vi_files . "' | sort"
  endif


  if !has('nvim') && has('python')
    let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
  endif
endif

"}}}
" UltiSnips plugin {{{

if has('python') || has('python3')
  let g:snips_author = 'Aldis Berjoza'
  let g:snips_author_email = 'aldis@berjoza.lv'
  let g:snips_author_homepage = 'https://aldis.berjoza.lv'

  let g:UltiSnipsExpandTrigger = "<tab>"
  let g:UltiSnipsJumpForwardTrigger = "<tab>"
  let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
  let g:UltiSnipsSnippetDirectories = ["custom_snippets"]
else
  call add(g:pathogen_disabled, 'ultisnips')
endif

"}}}
" Rails plugin {{{

let g:rails_no_abbreviations = 1
let g:rails_projections = {
      \ "app/validators/*_validator.rb": {
      \   "command":  "validator",
      \   "template": "class %SValidator < ActiveModel::Validator\nend",
      \   "test":     "spec/validators/%s_spec.rb"
      \ },
      \ "app/workers/*_worker.rb": {
      \   "command": "worker",
      \   "template": "class %SWorker\n  include Sidekiq::Worker\nend",
      \   "test":     "spec/workers/%s_spec.rb"
      \ },
      \ "app/policies/*_policy.rb": {
      \   "command":  "policy",
      \   "template": "class %SPolicy < ApplicationPolicy\n  class Scope < ::ApplicationPolicy::Scope\n    def resolve\n      # TODO\n      end\n    end\nend",
      \   "test":     "spec/policies/%s_policy_spec.rb"
      \ },
      \ "app/services/*.rb": {
      \   "command":  "service",
      \   "test":     "spec/services/%s_spec.rb"
      \ }
      \}

"}}}
" EasyGrep plugin {{{

let g:EasyGrepFileAssociations = realhome . '/.vim/EasyGrepFileAssociations'
let g:EasyGrepMode = 0
" let g:EasyGrepEveryMatch = 1
let g:EasyGrepSearchCurrentBufferDir = 0
" let g:EasyGrepInvertWholeWord = 1
let g:EasyGrepFileAssociationsInExplorer = 1
let g:EasyGrepRecursive = 1

"}}}
" gutentags plugin {{{

if has('tag_binary') && exists("ctags_progr")
  let g:gutentags_ctags_executable = ctags_progr
  let g:gutentags_cache_dir = vim_tmp . '/gutentags/' . ProjectName()
  call MkdirIfNeeded(g:gutentags_cache_dir, "p", 0700)
else
  call add(g:pathogen_disabled, 'easytags')
endif

"}}}
" VCScommand {{{

let g:VCSCommandDisableMenu = 1

"}}}
" TagBar {{{

let g:tagbar_ctags_bin = ctags_progr
let g:tagbar_autofocus = 1

"}}}
" Gitv {{{

let g:Gitv_OpenPreviewOnLaunch = 1

"}}}
" IdentGuides {{{

let g:indent_guides_start_level = 2
" let g:indent_guides_guide_size = 1
let g:indent_guides_auto_colors = 0

"}}}
" GnuPg {{{

let g:GPGPreferSign = 1
" let g:GPGDefaultRecipients = ['0x00000000']
let g:GPGUsePipes = 1

"}}}
" vim-ruby {{{

let g:rubycomplete_rails = 1

"}}}
" vim-rspec {{{

let g:rspec_command = "Dispatch rspec {spec}"

"}}}
" vim-reek {{{

let g:reek_on_loading = 0

"}}}
" go-vim {{{

" let g:go_snippet_engine = ""

"}}}
" syntastic {{{

let g:syntastic_python_pylint_post_args='--max-line-length=120'
let g:syntastic_python_flake8_args='--ignore=E501'

"}}}
" Pymode {{{

let g:pymode_rope = 0

"}}}
" gnupg {{{

let g:GPGExecutable='gpg2 --trust-model always'

"}}}
call pathogen#infect()
call ipi#inspect()

" CamlCaseMotion {{{
" This must be executed after pathogen#infect is called

call camelcasemotion#CreateMotionMappings(',')

"}}}


function! LoadGit()
  :IP fugitive gitv extradite
  :e!
  :delcommand LoadGit
endfunction
command! LoadGit :call LoadGit()


" Read man pages
if !has('nvim')
  runtime ftplugin/man.vim
endif

nmap <C-A><C-A>   <ESC>:call LoadIfNeeded(['ctrlp'])<CR>:CtrlPBuffer<CR>
nmap <C-A>        <ESC>:buffers<cr>:buffer<Space>
nmap <C-A>n       <ESC>:next<cr>
nmap <C-A>p       <ESC>:previous<cr>

nmap <Down>   <ESC>:previous<cr>
nmap <Up>     <ESC>:next<cr>
nmap <Right>  <ESC>:tabnext<cr>
nmap <Left>   <ESC>:tabprevious<cr>
" noremap ; l
" noremap l k
" noremap k j
" noremap j h
" noremap h :
" disabled ex mode
nnoremap Q <nop>


nmap <C-P>      <ESC>:call LoadIfNeeded(['ctrlp'])<CR>:CtrlP<CR>
nmap ,f         <ESC>:call LoadIfNeeded(['ctrlp'])<CR>:CtrlPRoot<CR>
nmap ,t         <ESC>:call LoadIfNeeded(['l9','fuzzyfinder'])<CR>:FufBufferTag<CR>
nmap <Leader>m  <ESC>:call LoadIfNeeded(['ctrlp', 'ctrlp-modified'])<CR>:CtrlPModified<CR>
" nmap <Leader>t  <ESC>:call LoadIfNeeded(['ctrlp', 'ctrlp-funky'])<CR>:CtrlPFunky<CR>
nmap <Leader>a  <ESC>:call LoadIfNeeded(['ctrlp', 'ctrlp-smarttabs'])<CR>:CtrlPSmartTabs<CR>
nmap ,T         <ESC>:call LoadIfNeeded(['l9','fuzzy_finder'])<CR>:FufBufferTagAll<CR>
nmap ,F         <ESC>:call LoadIfNeeded(['l9','fuzzy_finder'])<CR>:FufFile<CR>


map ,$ <plug>NERDCommenterToEOL
map ,a <plug>NERDCommenterAppend
map ,, <plug>NERDCommenterToggle
map ,d <plug>NERDCommenterAltDelims
map ,i <plug>NERDCommenterInvert
map ,m <plug>NERDCommenterMinimal
map ,n <plug>NERDCommenterNest
map ,s <plug>NERDCommenterSexy
map ,u <plug>NERDCommenterUncomment
map ,x <plug>NERDCommenterUncomment
map ,y <plug>NERDCommenterYank
map ,z <plug>NERDCommenterComment

nmap <leader>j :SplitjoinJoin<cr>
nmap <leader>s :SplitjoinSplit<cr>

if (g:os == 'Darwin')
  nmap <D-t>        <ESC>:tabnew<CR>
  nmap <D-w>        <ESC>:close<CR>
  nmap <D-S-left>   <ESC>:execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
  nmap <D-S-right>  <ESC>:execute 'silent! tabmove ' . (tabpagenr())<CR>
  nmap <D-left>     <ESC>:tabprevious<CR>
  nmap <D-right>    <ESC>:tabnext<CR>
endif

" http://vim.wikia.com/wiki/Capitalize_words_and_regions_easily
if (&tildeop)
  nmap gcw guw~l
  nmap gcW guW~l
  nmap gciw guiw~l
  nmap gciW guiW~l
  nmap gcis guis~l
  nmap gc$ gu$~l
  nmap gcgc guu~l
  nmap gcc guu~l
  vmap gc gu~l
else
  nmap gcw guw~h
  nmap gcW guW~h
  nmap gciw guiw~h
  nmap gciW guiW~h
  nmap gcis guis~h
  nmap gc$ gu$~h
  nmap gcgc guu~h
  nmap gcc guu~h
  vmap gc gu~h
endif

if (has("X11") && exists("$DISPLAY") && exists("$XAUTHORITY") && filereadable($XAUTHORITY)) || g:os == 'Darwin'
  " for copy/paste to work in Darwin you need to:
  "   brew install macvim
  " replace vim and vimdiff in /usr/bin with links to /usr/local/bin/gvim and gvimdiff
  " NOTE: Darwin only has one paste buffer
  vmap <F2> "*y
  nmap <F2> <ESC>"*p
  imap <F2> <ESC>"*p
  vmap <F3> "+y
  nmap <F3> <ESC>"+p
  imap <F3> <ESC>"+p
  if g:os == 'Darwin'
    imap <D-v> <ESC>"+p
    nmap <D-v> <ESC>"+p
    vmap <D-c> "+y
  endif
elseif exists("$DISPLAY") && exists("$XAUTHORITY") && filereadable($XAUTHORITY) && ProgrPath(['xclip']) != ''
  vmap <silent> <F2> :!xclip -i -f -selection primary<CR>
  nmap <silent> <F2> <ESC>:r!xclip -o -selection primary<CR>
  imap <silent> <F2> <ESC>:r!xclip -o -selection primary<CR>
  vmap <silent> <F3> :!xclip -i -f -selection clipboard<CR>
  nmap <silent> <F3> <ESC>:r!xclip -o -selection clipboard<CR>
  imap <silent> <F3> <ESC>:r!xclip -o -selection clipboard<CR>
else
  vmap <F2> :write! ~/tmp/.vim/vim_clip1<CR>
  nmap <F2> <ESC>:read ~/tmp/.vim/vim_clip1<CR>
  imap <F2> <ESC>:read ~/tmp/.vim/vim_clip1<CR>
  vmap <F3> :write! ~/tmp/.vim/vim_clip2<CR>
  nmap <F3> <ESC>:read ~/tmp/.vim/vim_clip2<CR>
  imap <F3> <ESC>:read ~/tmp/.vim/vim_clip2<CR>
endif

" F1 is mapped by ftplugins
" F2 is mapped above
" F3 is mapped above
nmap <F4>     <ESC>:bdelete<CR>
nmap <F5>     <ESC>:call LoadIfNeeded(['vcscommand'])<CR>:VCSVimDiff<CR>:VCSGotoOriginal<CR>
nmap <S-F5>   <ESC>:call LoadIfNeeded(['vcscommand'])<CR>:VCSDiff<CR>
nmap <F6>     <esc>:call LoadIfNeeded(['indent-guides'])<CR>:IndentGuidesToggle<CR>
nmap <F7><F7> <ESC>:setlocal invhlsearch<CR>
nmap <F7>     <ESC>:call ToggleSyntaxOrSpecChars()<CR>
nmap <F8>     <ESC>:call LoadIfNeeded(['tagbar'])<CR>:TagbarToggle<CR>
nmap <S-F8>   <ESC>:call LoadIfNeeded(['tagbar'])<CR>:TagbarOpenAutoClose<CR>
nmap <S-F9>   <ESC>:call RunCurrentSpecFile()<CR>
nmap <F9>     <ESC>:call RunNearestSpec()<CR>
nmap <F10>    <ESC>:NERDTreeToggle<CR>
nmap <F11>    <ESC>:set number!<CR>
nmap <F11>r   <ESC>:set relativenumber!<CR>
nmap <F12>    <ESC>:silent execute "! open \"" . expand('%:p:h') . "\""<CR>
nmap <S-F12>  <ESC>:silent execute "! open \"" . ProjectDir() . "\""<CR>
nmap <C-F12>  <ESC>:silent execute "! open \"" . ProjectDir() . "\""<CR>

if g:os == 'Darwin'
  au BufEnter /private/tmp/crontab.* setl backupcopy=yes
endif

" disable syntax hilighting for files over 256KB
" jump to previously edited file on buffer read
au BufReadPost *        if getfsize(bufname("%")) > 256*1024 |
\                         set syntax= |
\                       endif |
\                       if line("'\"") > 0 && line("'\"") <= line("$") |
\                         exe "normal! g`\"" |
\                       endif

let &viewdir = vim_cache . '/view'
call MkdirIfNeeded(&viewdir, 'p', 0700)


if filereadable('/etc/manjaro-release')
  " works pretty good with Manjaro default theme
  set background=dark
  colorscheme apprentice
elseif has('gui_running') == 1
  set background=dark
  colorscheme solarized
elseif g:os == 'Minix' && !$SSH_CLIENT
  set background=dark
  colorscheme universal-blue
else
  set background=dark
  colorscheme apprentice
  "colorscheme solarized
endif

set vb t_vb=

augroup VIMrc
  au!
  " Use visual flashing instead of beeping & disable visualbells
  au GUIEnter * set vb t_vb=
augroup END

set novisualbell


" WORKAROUNDS
if has('gui_running') == 1
  " faster syntax hilighting
  set regexpengine=1
end

" vim: textwidth=0 fdm=marker ts=2 sw=2 expandtab:
