DEFAULT_PLUGINS=nerdtree supertab ctrlp ack nerdcommenter ultisnips \
	easygrep camelcasemotion matchit a l9 fuzzyfinder indent-guides \
	ctrlp-modified fugitive writepath ctrlp-smarttabs ctrlp-funky \
	file-line xml_expander syntastic matchtag \
	ctrlp-py-matcher gutentags terraform
WORK_PLUGINS=${DEFAULT_PLUGINS} pymode salt yaml jinja nginx
RUBY_PLUGINS=ruby rspec dispatch rspec projectionist
RAILS_PLUGINS=${DEFAULT_PLUGINS} ${RUBY_PLUGINS} rails html5 nginx yaml slim
HOME_PLUGINS=${DEFAULT_PLUGINS} ${RUBY_PLUGINS} html5 yaml slim jinja pymode

BUNDLE_DIR=${HOME}/.vim/bundle
IPI_DIR=${HOME}/.vim/ipi
GET_PLUGIN=./plugin_update
FETCH_CMD=curl -L

default: pathogen Ipi ${DEFAULT_PLUGINS}

vue:
	${GET_PLUGIN} https://github.com/posva/vim-vue

cf3:
	${FETCH_CMD} -o "${HOME}/.vim/syntax/cf3.vim" https://raw.githubusercontent.com/neilhwatson/vim_cf3/70f73854f8c7346c3b665dee372bfac19fe9a25e/syntax/cf3.vim


work_stuff: pathogen Ipi ${WORK_PLUGINS}

rails_stuff: pathogen Ipi ${RAILS_PLUGINS}

ruby_stuff: pathogen Ipi ${RUBY_PLUGINS}

home_stuff: pathogen Ipi ${HOME_PLUGINS}

terraform:
	${GET_PLUGIN} https://github.com/hashivim/vim-terraform

slim:
	${GET_PLUGIN} https://github.com/slim-template/vim-slim

groovy:
	${GET_PLUGIN} https://github.com/modille/groovy.vim

ruby:
	${GET_PLUGIN} https://github.com/vim-ruby/vim-ruby

jinja:
	${GET_PLUGIN} https://github.com/lepture/vim-jinja

yaml:
	${GET_PLUGIN} https://github.com/stephpy/vim-yaml

salt:
	${GET_PLUGIN} https://github.com/saltstack/salt-vim

gutentags:
	${GET_PLUGIN} https://github.com/ludovicchabant/vim-gutentags

pymode:
	${GET_PLUGIN} https://github.com/python-mode/python-mode

go:
	${GET_PLUGIN} https://github.com/fatih/vim-go

django-plus:
	${GET_PLUGIN} https://github.com/tweekmonster/django-plus.vim

reek:
	${GET_PLUGIN} https://github.com/rainerborene/vim-reek

gnupg:
	${GET_PLUGIN} https://gitlab.com/graudeejs/vim-gnupg

rspec:
	${GET_PLUGIN} https://github.com/thoughtbot/vim-rspec

rubocop:
	${GET_PLUGIN} https://github.com/sodapopcan/vim-rubocop -d ${IPI_DIR}

javascript:
	@#${GET_PLUGIN} https://github.com/pangloss/vim-javascript
	${GET_PLUGIN} https://github.com/whitneyit/vim-javascript

elixir:
	${GET_PLUGIN} https://github.com/elixir-lang/vim-elixir

avr8bit:
	mkdir -p "${HOME}/.vim/syntax"
	${FETCH_CMD} -o "${HOME}/.vim/syntax/avr8bit.vim" "https://raw.githubusercontent.com/vim-scripts/avr8bit.vim/master/syntax/avr8bit.vim"

diff-enhanced:
	${GET_PLUGIN} https://github.com/chrisbra/vim-diff-enhanced -d ${IPI_DIR}

thesaurus:
	${GET_PLUGIN} https://github.com/beloglazov/vim-online-thesaurus -d ${IPI_DIR}

rubytest:
	${GET_PLUGIN} https://github.com/janx/vim-rubytest -d ${IPI_DIR}

html5:
	for dir in indent syntax; do \
		mkdir -p "${HOME}/.vim/$${dir}"; \
		${FETCH_CMD} -o "${HOME}/.vim/$${dir}/html.vim" "https://raw.githubusercontent.com/othree/html5.vim/master/$${dir}/html.vim"; \
	done

nginx:
	for dir in ftdetect indent syntax; do \
		mkdir -p "${HOME}/.vim/$${dir}"; \
		${FETCH_CMD} -o "${HOME}/.vim/$${dir}/nginx.vim" "http://hg.nginx.org/nginx/raw-file/default/contrib/vim/$${dir}/nginx.vim"; \
	done

matchtag:
	${GET_PLUGIN} https://github.com/gregsexton/MatchTag -d ${IPI_DIR}

markdown:
	${GET_PLUGIN} https://github.com/plasticboy/vim-markdown -d ${IPI_DIR}

xml_expander:
	${GET_PLUGIN} https://github.com/vim-scripts/XMLExpander -d ${IPI_DIR}

dispatch:
	${GET_PLUGIN} https://github.com/tpope/vim-dispatch

ctrlp-funky:
	${GET_PLUGIN} https://github.com/tacahiroy/ctrlp-funky -d ${IPI_DIR}

nerdtree:
	${GET_PLUGIN} https://github.com/scrooloose/nerdtree

nerdtree-tabs:
	${GET_PLUGIN} https://github.com/jistr/vim-nerdtree-tabs -d ${IPI_DIR}

vim-web-indent:
	${GET_PLUGIN} https://github.com/lukaszb/vim-web-indent
ctrlp-smarttabs:
	${GET_PLUGIN} https://github.com/DavidEGx/ctrlp-smarttabs -d ${IPI_DIR}
ctrlp-py-matcher:
	${GET_PLUGIN} https://github.com/FelikZ/ctrlp-py-matcher

writepath:
	${GET_PLUGIN} https://github.com/artnez/vim-writepath
	@#${GET_PLUGIN} https://github.com/artnze/vim-writepath

file-line:
	# ${GET_PLUGIN} https://github.com/bogado/file-line
	${GET_PLUGIN} https://github.com/dsiroky/file-line

syntastic:
	${GET_PLUGIN} https://github.com/scrooloose/syntastic

vdebug:
	${GET_PLUGIN} https://github.com/joonty/vdebug

syntax-range:
	${GET_PLUGIN} https://github.com/vim-scripts/SyntaxRange

signature:
	${GET_PLUGIN} https://github.com/kshenoy/vim-signature

splitjoin:
	${GET_PLUGIN} https://github.com/AndrewRadev/splitjoin.vim

cucumber:
	${GET_PLUGIN} https://github.com/tpope/vim-cucumber

edit_similar:
	${GET_PLUGIN} https://github.com/vim-scripts/EditSimilar

surround:
	${GET_PLUGIN} https://github.com/tpope/vim-surround

extradite:
	${GET_PLUGIN} https://github.com/int3/vim-extradite -d ${IPI_DIR}

supertab:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/SuperTab
	${GET_PLUGIN} https://github.com/ervandew/supertab

youcompleteme:
	echo "You complete me is to complicated to install with make"
	@# ${GET_PLUGIN} https://github.com/Valloric/YouCompleteMe
	@## https://github.com/Valloric/YouCompleteMe#freebsdopenbsd
	@## https://github.com/Valloric/YouCompleteMe#fedora-linux-x64

ctrlp:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/ctrlp.vim -d ${IPI_DIR}
	${GET_PLUGIN} https://github.com/ctrlpvim/ctrlp.vim -d ${IPI_DIR}

ctrlp-modified:
	${GET_PLUGIN} https://github.com/jasoncodes/ctrlp-modified.vim -d ${IPI_DIR}

ack:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/ack.vim -d ${IPI_DIR}
	${GET_PLUGIN} https://github.com/mileszs/ack.vim

nerdcommenter:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/The-NERD-Commenter
	@# ${GET_PLUGIN} https://github.com/scrooloose/nerdcommenter
	@# ${GET_PLUGIN} https://github.com/ervandew/nerdcommenter
	${GET_PLUGIN} https://gitlab.com/graudeejs/nerdcommenter

fugitive:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/fugitive.vim -d ${IPI_DIR}
	${GET_PLUGIN} https://github.com/tpope/vim-fugitive

rails:
	${GET_PLUGIN} https://github.com/tpope/vim-rails

fuzzyfinder:
	${GET_PLUGIN} https://github.com/vim-scripts/FuzzyFinder -d ${IPI_DIR}
	@## This one could be better than original
	# ${GET_PLUGIN} https://github.com/teki/FuzzyFinder -d ${IPI_DIR}

l9:
	${GET_PLUGIN} https://github.com/vim-scripts/L9 -d ${IPI_DIR}
	@# ${GET_PLUGIN} https://github.com/vim-scripts/L9 -d ${IPI_DIR}

ultisnips:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/UltiSnips
	${GET_PLUGIN} https://github.com/sirver/ultisnips

clang_complete:
	${GET_PLUGIN} https://github.com/Rip-Rip/clang_complete -d ${IPI_DIR}
	@#${GET_PLUGIN} https://github.com/Schmallon/clang_complete -d ${IPI_DIR}

vcscommand:
	${GET_PLUGIN} https://github.com/vim-scripts/vcscommand.vim

gitv:
	${GET_PLUGIN} https://github.com/gregsexton/gitv -d ${IPI_DIR}

easygrep:
	${GET_PLUGIN} https://github.com/vim-scripts/EasyGrep -d ${IPI_DIR}

dbext:
	${GET_PLUGIN} https://github.com/bogdan/dbext -d ${IPI_DIR}

camelcasemotion:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/camelcasemotion
	${GET_PLUGIN} https://github.com/bkad/CamelCaseMotion

taglist:
	${GET_PLUGIN} https://github.com/vim-scripts/taglist.vim -d ${IPI_DIR}

a:
	@# ${GET_PLUGIN} https://github.com/vim-scripts/a.vim -d ${IPI_DIR}
	${GET_PLUGIN} https://gitlab.com/graudeejs/a.vim -d ${IPI_DIR}

matchit:
	${GET_PLUGIN} https://gitlab.com/graudeejs/vim-matchit -d ${IPI_DIR}

indent-guides:
	${GET_PLUGIN} https://github.com/nathanaelkane/vim-indent-guides -d ${IPI_DIR}

Ipi: ${HOME}/.vim/autoload ${BUNDLE_DIR} ${IPI_DIR}
	@# ${FETCH_CMD} -o "${HOME}/.vim/autoload/ipi.vim" "https://raw.github.com/vim-scripts/ipi/HEAD/plugin/ipi.vim"
	${FETCH_CMD} -o "${HOME}/.vim/autoload/ipi.vim" "https://raw.github.com/jceb/vim-ipi/HEAD/autoload/ipi.vim"

pathogen: ${HOME}/.vim/autoload ${BUNDLE_DIR}
	${FETCH_CMD} -o "${HOME}/.vim/autoload/pathogen.vim" "https://raw.github.com/tpope/vim-pathogen/HEAD/autoload/pathogen.vim"

unbundle: ${HOME}/.vim/autoload ${BUNDLE_DIR}
	${FETCH_CMD} -o "${HOME}/.vim/autoload/unbundle.vim" "https://raw.github.com/sunaku/vim-unbundle/HEAD/unbundle.vim"

tagbar:
	${GET_PLUGIN} https://github.com/majutsushi/tagbar -d ${IPI_DIR}

projectionist:
	${GET_PLUGIN} https://github.com/tpope/vim-projectionist

help:
	@echo ${ALL_PLUGINS} pathogen Ipi

${BUNDLE_DIR}:
	mkdir -p ${BUNDLE_DIR}

${IPI_DIR}:
	mkdir -p ${IPI_DIR}

${HOME}/.vim/autoload:
	mkdir -p ${HOME}/.vim/autoload
