" Vim colorscheme
" Maintainer:   Aldis Berjoza <graudeejs@yandex.ru>
" Last Change:  2012.11.09

" First remove all existing highlighting.
hi clear
if exists("syntax_on")
  syntax reset
endif

" Terminal colors
" color0:  #000000
" color8:  #606060
" color1:  #FF0000
" color9:  #FF3030
" color2:  #00800a
" color10: #00FF00
" color3:  #E7811B
" color11: #AFAF00
" color4:  #000060
" color12: #5050FF
" color5:  #800080
" color13: #FF00FF
" color6:  #008080
" color14: #00FFFF
" color7:  #909090
" color15: #FFFFFF
"
" background:       #FFF8DC
" foreground:       #202020
" underlineColor:   #5070ff
" cursorColor:      #C69948


if $WINDOWID != '' || $SSH_TTY != ''
  hi Normal     cterm=NONE      ctermfg=NONE            ctermbg=NONE
else
  hi Normal     cterm=NONE      ctermfg=Black           ctermbg=White
endif

hi Error        cterm=NONE      ctermfg=White           ctermbg=Red
hi Visual       cterm=REVERSE
hi Statement    cterm=NONE      ctermfg=DarkBlue        ctermbg=NONE
hi Comment      cterm=NONE      ctermfg=DarkGray        ctermbg=NONE
hi Constant     cterm=NONE      ctermfg=DarkGreen       ctermbg=NONE
hi PreProc      cterm=NONE      ctermfg=DarkCyan        ctermbg=NONE
hi Folded       cterm=NONE      ctermfg=Black           ctermbg=White
hi Type         cterm=NONE      ctermfg=DarkBlue        ctermbg=NONE
hi Identifier   cterm=NONE      ctermfg=NONE            ctermbg=NONE
hi Special      cterm=NONE      ctermfg=DarkMagenta     ctermbg=NONE
hi VertSplit    cterm=REVERSE   ctermfg=NONE            ctermbg=NONE
hi StatusLine   cterm=REVERSE   ctermfg=NONE            ctermbg=NONE
hi StatusLineNC cterm=REVERSE   ctermfg=NONE            ctermbg=Yellow
hi SpecialKey   cterm=NONE      ctermfg=White           ctermbg=DarkMagenta
hi Search       cterm=REVERSE
hi SpellBad                     ctermfg=White           ctermbg=Red
hi LineNr       cterm=REVERSE   ctermfg=NONE            ctermbg=NONE

hi DiffAdd      cterm=NONE      ctermfg=White           ctermbg=DarkGreen
hi DiffDelete   cterm=NONE      ctermfg=White           ctermbg=DarkRed
hi DiffText     cterm=NONE      ctermfg=Black           ctermbg=Yellow

hi TabLineSel   cterm=UNDERLINE ctermfg=NONE            ctermbg=NONE
hi TabLine      cterm=REVERSE   ctermfg=NONE            ctermbg=NONE
hi TabLineFill  cterm=NONE      ctermfg=NONE            ctermbg=Black

hi NonText      cterm=NONE      ctermfg=Cyan            ctermbg=DarkCyan

hi Pmenu        cterm=NONE      ctermfg=White           ctermbg=DarkGray
hi PmenuSel     cterm=BOLD      ctermfg=White           ctermbg=Gray
hi PmenuSbar    cterm=NONE      ctermfg=White           ctermbg=DarkGray
hi PmenuThumb   cterm=NONE      ctermfg=White           ctermbg=Gray

hi MoreMsg      cterm=BOLD      ctermfg=none            ctermbg=NONE
hi ModeMsg      cterm=BOLD      ctermfg=none            ctermbg=NONE
hi MatchParen   cterm=NONE      ctermfg=Black           ctermbg=Cyan

hi IndentGuidesOdd      cterm=NONE ctermbg=DarkMagenta
hi IndentGuidesEven     cterm=NONE ctermbg=DarkGray

" http://vim.wikia.com/wiki/Configuring_the_cursor
" Fix cursor color for GUI
hi Cursor                     guifg=#ffff00     guibg=#00A000


hi Normal       gui=NONE      guifg=#202020     guibg=#FFF8DC

hi Error        gui=NONE      guifg=#ffffff     guibg=#FF3030
hi Visual       gui=REVERSE
hi Statement    gui=NONE      guifg=#000060     guibg=#fff8dc
hi Comment      gui=NONE      guifg=#606060     guibg=#fff8dc
hi Constant     gui=NONE      guifg=#00800a     guibg=#fff8dc
hi PreProc      gui=NONE      guifg=#008080     guibg=#fff8dc
hi Folded       gui=NONE      guifg=#000000     guibg=#ffffff
hi Type         gui=NONE      guifg=#000060     guibg=#fff8dc
hi Identifier   gui=NONE      guifg=#202020     guibg=#fff8dc
hi Special      gui=NONE      guifg=#800080     guibg=#fff8dc
hi VertSplit    gui=REVERSE   guifg=#202020     guibg=#fff8dc
hi StatusLine   gui=REVERSE   guifg=#202020     guibg=#fff8dc
hi StatusLineNC gui=REVERSE   guifg=#202020     guibg=#afaf00
hi SpecialKey   gui=NONE      guifg=#ffffff     guibg=#800080
hi Search       gui=REVERSE
hi SpellBad                   guifg=#ffffff     guibg=#FF3030
hi LineNr       gui=REVERSE   guifg=#202020     guibg=#fff8dc

hi DiffAdd      gui=NONE      guifg=#ffffff     guibg=#00800a
hi DiffDelete   gui=NONE      guifg=#ffffff     guibg=#FF0000
hi DiffText     gui=NONE      guifg=#000000     guibg=#afaf00

hi TabLineSel   gui=UNDERLINE guifg=#202020     guibg=#fff8dc
hi TabLine      gui=REVERSE   guifg=#202020     guibg=#fff8dc
hi TabLineFill  gui=NONE      guifg=#202020     guibg=#000000

hi NonText      gui=NONE      guifg=#00ffff     guibg=#008080

hi Pmenu        gui=NONE      guifg=#ffffff     guibg=#606060
hi PmenuSel     gui=BOLD      guifg=#ffffff     guibg=#909090
hi PmenuSbar    gui=NONE      guifg=#ffffff     guibg=#606060
hi PmenuThumb   gui=NONE      guifg=#ffffff     guibg=#909090

hi MoreMsg      gui=BOLD      guifg=#202020     guibg=#fff8dc
hi ModeMsg      gui=BOLD      guifg=#202020     guibg=#fff8dc
hi MatchParen   gui=NONE      guifg=#000000     guibg=#00ffff

hi IndentGuidesOdd      gui=NONE guibg=#800080
hi IndentGuidesEven     gui=NONE guibg=#606060
