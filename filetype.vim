au BufRead,BufNewFile /usr/local/nginx/conf/* setlocal ft=nginx
au BufRead,BufNewFile *.coffee setlocal ft=coffee
au BufRead,BufNewFile *.stpl setlocal ft=smarty
au BufRead,BufNewFile *.json setlocal ft=json
au BufRead,BufNewFile *.axlsx setlocal ft=ruby
au BufRead,BufNewFile Gemfile setlocal ft=Gemfile syn=ruby
au BufRead,BufNewFile config.ru setlocal ft=ruby
au BufRead,BufNewFile Jenkinsfile setlocal ft=groovy
au BufRead,BufNewFile *.cf setlocal ft=cf3
au BufRead,BufNewFile *.nix setlocal ft=nix
